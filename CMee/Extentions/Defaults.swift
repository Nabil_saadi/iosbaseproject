//
//  Defaults.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import SwiftyUserDefaults
import Foundation
extension DefaultsKeys {
    var IS_USER_LOGGED_IN: DefaultsKey<Int?> { .init("IS_USER_LOGGED_IN", defaultValue: 0) }
    var  LANG : DefaultsKey<String?>{ .init("LANG", defaultValue: "en") }
}
