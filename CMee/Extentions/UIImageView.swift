//
//  UIImageView.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    @IBInspectable var localized : Bool {
        set {
            if UIView.isRTL() {
                self.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
        }

        get {
            return true
        }
    }
}

