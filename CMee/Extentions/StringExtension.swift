//
//  StringExtension.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import UIKit

extension String {
    
    var removeWhiteSpace: String {
           return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
       }
       
       var removeNull: String {
           if self.isEmpty || self.count <= 0 {
               return ""
           }
           return self
       }
       
       var floatValue: Float {
           return (self as NSString).floatValue
       }
    
}
