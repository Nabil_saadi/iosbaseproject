//
//  UIViewExtention.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//


import Foundation
import UIKit

extension UIView {
    
    /**
     Set a shadow on a UIView.
     - parameters:
     - color: Shadow color, defaults to black
     - opacity: Shadow opacity, defaults to 1.0
     - offset: Shadow offset, defaults to zero
     - radius: Shadow radius, defaults to 0
     - viewCornerRadius: If the UIView has a corner radius this must be set to match
     */
    func flip() {
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    static func isRTL() -> Bool {
        //print("isRTL", appearance().semanticContentAttribute == .forceRightToLeft)
        return appearance().semanticContentAttribute == .forceRightToLeft
    }
    func setRoundedWithBorderColor(_ color: UIColor!, width: CGFloat!) {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.masksToBounds = true
    }
    
    
    func setShadowWithColor(color: UIColor?, opacity: Float?, offset: CGSize?, radius: CGFloat, scale: CGFloat?) {
        //layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: viewCornerRadius ?? 0.0).CGPath
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = radius ?? 0
        layer.masksToBounds = false
        
        //layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        //layer.shouldRasterize = true
        layer.rasterizationScale = 0.5 //UIScreen.main.scale
    }
    func setShadowWithColorAndCorner(color: UIColor?, opacity: Float?, offset: CGSize?, radius: CGFloat, viewCornerRadius: CGFloat?) {
        //layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: viewCornerRadius ?? 0.0).cgPath
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = radius ?? 0
        
       // layer.rasterizationScale = 0.5
    }
    func setShadowWithColorAndSpesidficCorner(color: UIColor?, opacity: Float?, offset: CGSize?, radius: CGFloat, viewCornerRadius: CGFloat?,corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: viewCornerRadius!, height: viewCornerRadius!))
        layer.shadowPath = path.cgPath
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = radius ?? 0
    }
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: "pulse")
    }
    
    func shake() {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = 0.1
        shake.repeatCount = 2
        shake.autoreverses = true
        let fromPoint = CGPoint(x: center.x - 5, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)
        let toPoint = CGPoint(x: center.x + 5, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)
        shake.fromValue = fromValue
        shake.toValue = toValue
        layer.add(shake, forKey: "position")
    }
    func rounded(corners: UIRectCorner , cornerRadius: Double) {
        var TempCorner = CACornerMask()
        if corners.contains(UIRectCorner.bottomRight) {
            TempCorner.insert(.layerMaxXMaxYCorner)
        }
        if corners.contains(UIRectCorner.bottomRight) {
            TempCorner.insert(.layerMaxXMaxYCorner)
        }
        if corners.contains(UIRectCorner.bottomRight) {
            TempCorner.insert(.layerMaxXMaxYCorner)
        }
        if corners.contains(UIRectCorner.bottomRight) {
            TempCorner.insert(.layerMaxXMaxYCorner)
        }

        self.layoutIfNeeded()
        self.layer.cornerRadius = CGFloat(cornerRadius)
        //self.clipsToBounds = true
        self.layer.maskedCorners = TempCorner
    }
    func round(corners: UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}





