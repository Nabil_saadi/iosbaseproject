//
//  UILabel.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import UIKit
extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
        
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(labelSize.width)
    }

    @IBInspectable var FontStyle : NSNumber {
        set {
            switch newValue {
            case 0:
                self.font = customFonts.regularFont
               // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case 1:
                self.font = customFonts.mediumFont
                //self.fon
               // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case 2:
                self.font = customFonts.boldFont
               // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case 3:
                self.font = customFonts.smallFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            default:
                break
            }
        }
        
        get {
            return 0
        }
    }
    @IBInspectable var FontType : String {
        set {
            switch newValue {
            case FONT_TYPES.REGULAR_VS.rawValue:
                self.font = customFonts.verysmallRegularFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
                
            case FONT_TYPES.REGULAR_S.rawValue:
                self.font = customFonts.smallRegularFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.REGULAR_M.rawValue:
            self.font = customFonts.mediumRegularFont
            // self.textColor = UIColor.white
            self.text = self.text?.localized()
            break
            case FONT_TYPES.REGULAR_L.rawValue:
                self.font = customFonts.largeRegularFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.REGULAR_XL.rawValue:
                self.font = customFonts.xlargeRegularFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.REGULAR_XXL.rawValue:
                self.font = customFonts.xxlargeRegularFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
                
            case FONT_TYPES.BOLD_S.rawValue:
                self.font = customFonts.smallBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.BOLD_M.rawValue:
                self.font = customFonts.mediumBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.BOLD_L.rawValue:
                self.font = customFonts.largeBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.BOLD_XL.rawValue:
                self.font = customFonts.xlargeBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.BOLD_XXL.rawValue:
                self.font = customFonts.xxlargeBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_VS.rawValue:
                self.font = customFonts.verysmallSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_S.rawValue:
                self.font = customFonts.smallSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_M.rawValue:
                self.font = customFonts.mediumSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_L.rawValue:
                self.font = customFonts.largeSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_XL.rawValue:
                self.font = customFonts.xlargeSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            case FONT_TYPES.SEMIBOLD_XXL.rawValue:
                self.font = customFonts.xxlargeSemiBoldFont
                // self.textColor = UIColor.white
                self.text = self.text?.localized()
                break
            default:
                break
            }
        }
        
        get {
            return ""
        }
    }
    @IBInspectable var localized : Bool {
        set {
            self.textAlignment = UIView.isRTL() ? .right : .left
        }
        
        get {
            return true
        }
    }
}

enum Styles {
    case normalWhite
    case mediumWhite
    case boldWhite
    case normal
    case medium
    case bold
    
}

