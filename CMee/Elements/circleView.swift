//
//  circleView.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import UIKit
class circleView : UIView {
    var viewLabel: String! {
        didSet {
            
        }
    }
    
    required init(coder aDecoder: NSCoder!)  {
        super.init(coder: aDecoder)!
        
        //println("init with coder, viewLabel = \(self.viewLabel)")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //self.layoutIfNeeded()
        self.round(corners: [.bottomLeft,.topLeft , .bottomRight , .topRight], radius: self.frame.height/2)
        if self.tag == 1 {
            self.setShadowWithColorAndCorner(color: UIColor.black, opacity: 0.5, offset: CGSize(width: 0, height: 0), radius: 6, viewCornerRadius: 0)
        }
    }
}
