//
//  ShadowView.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
class ShadowView : UIView {
    var viewLabel: String! {
        didSet {
            
        }
    }
    
    required init(coder aDecoder: NSCoder!)  {
        super.init(coder: aDecoder)!
        self.setShadowWithColorAndCorner(color: UIColor.black, opacity: 0.2 , offset: CGSize(width: 0, height: 0), radius: 4, viewCornerRadius: 20)
        //println("init with coder, viewLabel = \(self.viewLabel)")
    }
}
