//
//  DateField.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
class DateField: UITextField
{
    
    lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        return datePicker
    }()
    var selectedDate : Date?
    lazy var toolBar: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addDatePicker()
        text = DateUtilities.dateToStringWithFormat(date: Date() ,  format : "YYYY-MM-dd" )
    }
    
    func addDatePicker() {
        inputAccessoryView = toolBar
        inputView = datePicker
        datePicker.addTarget(self, action: #selector(onChangeDatePicker), for: .valueChanged)
    }
    @objc func onChangeDatePicker(){
        selectedDate = datePicker.date
        text = DateUtilities.dateToStringWithFormat(date: selectedDate! ,  format : "YYYY-MM-dd")
    }
    @objc func donedatePicker(){
        selectedDate = datePicker.date
        text = DateUtilities.dateToStringWithFormat(date: selectedDate! ,  format : "YYYY-MM-dd")
        endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        endEditing(true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        addDatePicker()
        text = DateUtilities.dateToStringWithFormat(date: Date()  ,  format : "YYYY-MM-dd")
    }
    
}
