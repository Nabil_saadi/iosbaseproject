//
//  PickerField.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
protocol PickerFieldDelegate {
    func didSelect(image : URL)
    func didSelect(index:Int , field : PickerField)
}
class PickerField: UITextField
{
    // this is boolean for change text when user select item befor click on done btn
    var setText = true
    var selectedRow  = 0
    var PickerStringData  = [String](){
        didSet{
            hasImage = false
            inputAccessoryView = toolBar
            
            inputView = Picker
            if PickerDataArray.count != 0 {
                self.text  = PickerStringData[selectedRow].localized()
            }
            
        }
    }
    var delegatePicker : PickerFieldDelegate?
    var PickerDataArray = [DataPickerModel](){
        didSet{
            hasImage = true
            inputAccessoryView = toolBar
            
            inputView = Picker
          
        }
    }
    var hasImage : Bool = false
    lazy var Picker : UIPickerView =
        {
            var Picker = UIPickerView()
            Picker.backgroundColor = UIColor.white
            Picker.delegate = self
            return Picker
        }()
    
    
     lazy var toolBar: UIToolbar = {
     
     let toolbar = UIToolbar()
     
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(donedatePicker))
     
     let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(cancelDatePicker))
     
     
     
     toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
     
     toolbar.sizeToFit()
     
     return toolbar
     
     }()
    
    
    @objc func donedatePicker(){
        if hasImage {
            self.text  = PickerDataArray[selectedRow].Name
        }
        else {
            self.text  = PickerStringData[selectedRow]
        }
        
        self.delegatePicker?.didSelect(index: selectedRow , field: self)
        endEditing(true)
        
    }
    
    
    
    @objc func cancelDatePicker() {
        
        //self.text = ""
        
        endEditing(true)
        
    }
    
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        //addPicker()
    }
    
    
    
    func addPicker() {
        
         inputAccessoryView = toolBar
        
        inputView = Picker
        
//        PickerDataArray.append(DataPickerModel(Id: 0, Name: "Dubai", Img: UIImage(named: "home.png")!))
//        PickerDataArray.append(DataPickerModel(Id: 0, Name: "hind", Img: UIImage(named: "tweet.png")!))
//        PickerDataArray.append(DataPickerModel(Id: 0, Name: "syria", Img: UIImage(named: "tweet.png")!))
//        PickerDataArray.append(DataPickerModel(Id: 0, Name: "lebanon", Img: UIImage(named: "tweet.png")!))
//        PickerDataArray.append(DataPickerModel(Id: 0, Name: "Dubai", Img: UIImage(named: "tweet.png")!))
    }
    
    
    
    required init?(coder aDecoder: NSCoder)
    {
        
        super.init(coder: aDecoder)
        
       // addPicker()
        
    }
    
    
    
    
    
}
extension PickerField : UIPickerViewDelegate , UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if hasImage {
            return PickerDataArray.count
        }
        else {
            return PickerStringData.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return PickerDataArray[row].Name.localized()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRow = row
        if setText
        {
        if hasImage {
            self.text  = PickerDataArray[selectedRow].Name.localized()
        }
        else {
            self.text  = PickerStringData[selectedRow].localized()
        }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 100.0
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        switch hasImage{
        case false:
            
            var label:UILabel
            if let v = view as? UILabel
            {
                label = v
            }
            else
            {
                label = UILabel()
            }
            label.textAlignment = .center
            label.font = customFonts.mediumFont
            label.text = PickerStringData[row]
            label.sizeToFit()
            return label
        case true:
            let allViewsInXibArray = Bundle.main.loadNibNamed("CustomPickerViewCell", owner: self, options: nil)
            let myView = allViewsInXibArray?.first as! CuatomPickerViewCell
            myView.pickerRowName.text = PickerDataArray[row].Name
            myView.PickerRowImage.kf.setImage(with: PickerDataArray[row].Img)
            return myView
        }
    }
    func setSelectedRow(index : Int )
    {
        selectedRow = index
        self.Picker.selectRow(index , inComponent: 0, animated: true)
    }
    
}
