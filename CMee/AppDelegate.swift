//
//  AppDelegate.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import UIKit
import Localize_Swift
import SwiftyUserDefaults
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        changLanguage()
        return true
    }
    
    func changLanguage()
    {
        Localize.setCurrentLanguage("ar")
        appLanguage = .ar
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        Localizer.changeLanguage()
        Defaults[\.LANG] = "ar"
    }
    
    // MARK: UISceneSession Lifecycle
//
//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }


}

