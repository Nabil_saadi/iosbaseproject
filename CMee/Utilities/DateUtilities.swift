//
//  DateUtilities.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation

import Foundation
class DateUtilities {
    static func dateToString(date:Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        //        if UIView.isRTL() {
        //            formatter.locale = Locale(identifier:"ar")
        //        }
        //formatter.dateStyle = .medium
       // formatter.timeStyle = .none
        print(formatter.string(from: date))
        return formatter.string(from: date)
    }
    
    static func dateToStringWithFormat(date:Date , format : String ) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format //"dd-MM-YYYY"
        //        if UIView.isRTL() {
        //            formatter.locale = Locale(identifier:"ar")
        //        }
        //formatter.dateStyle = .medium
        // formatter.timeStyle = .none
        //print(formatter.string(from: date))
        return formatter.string(from: date)
    }
    static func currentUTC() -> String {
        let date = Date()
        print(date)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        formatter.timeZone = TimeZone(identifier: "UTC")
        print(formatter.string(from: date))
        return formatter.string(from: date)
    }
    static func utc2String(date:String) -> String
    {
        // create dateFormatter with UTC time format
     
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let DATE = dateFormatter.date(from: date)// create   date from string
        if DATE == nil
        {
           return Date2DateStringWithFormat(date: date , Fromformat : "yyyy-MM-dd'T'HH:mm:ssZ" , ToFormat : "yyyy-MM-dd")
        }
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: DATE ?? Date())
       
        return timeStamp
    }
    
    static func utc2TimeString(date:String) -> String
    {
        // create dateFormatter with UTC time format
     
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let DATE = dateFormatter.date(from: date)// create   date from string
        if DATE == nil
        {
           return Date2DateStringWithFormat(date: date , Fromformat : "yyyy-MM-dd'T'HH:mm:ssZ" , ToFormat : "yyyy-MM-dd")
        }
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = "HH:mm"
        
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: DATE ?? Date())
       
        return timeStamp
    }
    
    
    static func utc2DateString(date:String) -> String {
        // create dateFormatter with UTC time format
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let DATE = dateFormatter.date(from: date )// create   date from string
        if DATE != nil
        {
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            dateFormatter.timeZone = TimeZone.current
            let timeStamp = dateFormatter.string(from: DATE! )
            return timeStamp
        }
        else
        {
           return utc2DateStringWithFormat(date: date , format: "dd-MMM-yyyy")
        }
        // change to a readable time format and change to local time zonee
    }
    
    static func utc2DateStringWithFormat(date:String , format : String
        ) -> String {
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: date )// create   date from string
        
        // change to a readable time format and change to local time zone "dd-MMM-yyyy"
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date ?? Date())
        return timeStamp
    }
    
    static func Date2DateStringWithFormat(date:String , Fromformat : String , ToFormat : String)
         -> String {
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Fromformat
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        let date = dateFormatter.date(from: date )// create   date from string
        
        // change to a readable time format and change to local time zone "dd-MMM-yyyy"
        dateFormatter.dateFormat = ToFormat
        dateFormatter.timeZone = TimeZone.current
        let timeStamp = dateFormatter.string(from: date ?? Date())
        print(timeStamp)
        return timeStamp
    }
    
    
  static  func DiffrenceDay(start : String , end : String) -> Int
       {
           print(start)
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
           //dateFormatter.locale = Locale(identifier: "en_US_POSIX")
           dateFormatter.timeZone = TimeZone.current
           var startTime = dateFormatter.date(from: start)!
           print(startTime)
           
           let userCalendar = Calendar.current
           let requestedComponent: Set<Calendar.Component> = [.month,.day]
           
           var endTime = dateFormatter.date(from: end)
           if endTime == nil
           {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            endTime = dateFormatter.date(from: end)!
            print(endTime)
            }
            startTime = userCalendar.startOfDay(for: startTime)
            endTime = userCalendar.startOfDay(for: endTime!)
           
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime, to: endTime!)
           //print( "\(timeDifference.month) Months \(timeDifference.day) Days \(timeDifference.minute) Minutes \(timeDifference.second) Seconds")
           //print(timeDifference)
           let day = timeDifference.day
           return day!
        
       
           //  return Int64(result)
       }
    
    
}
