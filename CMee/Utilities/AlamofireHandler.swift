//
//  AlamofireHandler.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//


import UIKit
import Alamofire
import SystemConfiguration
import AlamofireActivityLogger
let API_FILE_NAME   = "fileName"
let API_IMAGE_NAME  = "imageName"
let API_IMAGE       = "image"

typealias APIBlock = (_ error: NSError?, _ response: AnyObject?) -> Void

class AlamofireHandler: NSObject {
    
    // MARK: - Properties
    
    static let arrContentType = ["application/json", "text/json", "text/javascript", "text/html"]
    
    // MARK: - GET Methods

    
    static func GET_GENERAL(_ url: String!, completion: APIBlock?) {
        if(self.isConnectedToNetwork()) {
            NSLog("API: GET_GENERAL\nURL: \(url)")
            
            Alamofire.request(url)
                .validate(contentType: arrContentType).log()
                .responseJSON { response in
                    NSLog("API Response: \(response)")
                    
                    if let JSON = response.result.value {
                        completion?(nil, JSON as AnyObject)
                    } else {
                        completion?(response.result.error! as NSError, nil)
                    }
            }
        } else {
            completion?(self.getErrorInternetConnection(), nil)
        }
    }
    
    // MARK:- POST
    
  
    
    // MARK: - Header
    
   
    
    static func getErrorInternetConnection() -> NSError {
        return NSError(domain: "Internet Connection Error", code: -1, userInfo: [NSLocalizedDescriptionKey: "connection error"])
    }
    
    static func getErrorSomethingWentWrong() -> NSError {
        return NSError(domain: "Error in API call", code: -1, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, please try again"])
    }
    
    // MARK: - Reachability Methods
    
//    static func isConnectedToNetwork() -> Bool {
//        let network = NetworkReachabilityManager()
//        return (network?.isReachable)!
//    }
    
    static func isConnectedToNetwork() -> Bool {
//        guard let flags = getFlags() else { return false }
//        let isReachable = flags.contains(.reachable)
//        let needsConnection = flags.contains(.connectionRequired)
//        return (isReachable && !needsConnection)
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
    
//    static func getFlags() -> SCNetworkReachabilityFlags? {
//        guard let reachability = ipv4Reachability() ?? ipv6Reachability() else {
//            return nil
//        }
//        var flags : SCNetworkReachabilityFlags = []
//        if !SCNetworkReachabilityGetFlags(reachability, &flags) {
//            return nil
//        }
//        return flags
//    }
    
//    static func ipv6Reachability() -> Bool? {
//        var zeroAddress = sockaddr_in6()
//        zeroAddress.sin6_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin6_family = sa_family_t(AF_INET6)
////        BIG - J
//        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {_ in
////            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
////            SCNetworkReachabilityCreateWithAddress(nil)
//            SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, UnsafePointer($0))
//        }) else {
//            return nil
//        }
//
//        return defaultRouteReachability
//    }
    
//    static func ipv4Reachability() -> SCNetworkReachability? {
//        var zeroAddress = sockaddr_in()
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//
//        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {_ in
//                SCNetworkReachabilityCreateWithAddress(nil)
////            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
//        }) else {
//            return nil
//        }
//
//        return defaultRouteReachability
//    }
}
