//
//  Constants.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//


import Foundation
import UIKit
enum FONTS : String {
    case regular = "Cairo"
    case bold = "Cairo-Bold"
    case medium = "Cairo-SemiBold"
}

enum FONT_TYPES : String {
    /*case SMALL = "small"
    case REGULAR = "regular"
 case BOLD = "bold"
  case MEDIUM = "medium"
  case BIG = "big"
  case SEMIBOLD = "semibold"
  case SEMI = "semi"
    */
    case REGULAR_VS = "r_vs"
    case REGULAR_S = "r_s"
    case REGULAR_M = "r_m"
    case REGULAR_L = "r_l"
    case REGULAR_XL = "r_xl"
    case REGULAR_XXL = "r_xxl"
    case BOLD_S = "b_s"
    case BOLD_M = "b_m"
    case BOLD_L = "b_l"
    case BOLD_XL = "b_xl"
    case BOLD_XXL = "b_xxl"
    case SEMIBOLD_VS = "s_vs"
    case SEMIBOLD_S = "s_s"
    case SEMIBOLD_M = "s_m"
    case SEMIBOLD_L = "s_l"
    case SEMIBOLD_XL = "s_xl"
    case SEMIBOLD_XXL = "s_xxl"
    
    
 
}
let APPDELEGATE: AppDelegate    = UIApplication.shared.delegate as? AppDelegate ?? AppDelegate()
class customFonts {
    static var smallFont = UIFont(name: FONTS.regular.rawValue , size: 14)
    static var regularFont = UIFont(name: FONTS.regular.rawValue , size: 18)
    static var mediumFont = UIFont(name: FONTS.regular.rawValue , size: 18)
    static var boldFont = UIFont(name: FONTS.bold.rawValue , size: 18)
  
    static var placeHolderFont = UIFont(name: FONTS.bold.rawValue , size: 15)
    static var semiBoldFont = UIFont(name: FONTS.medium.rawValue , size: 25)
    static var semiFont = UIFont(name: FONTS.regular.rawValue , size: 25)
    
    static var verysmallRegularFont =  UIFont(name: FONTS.regular.rawValue , size: 12)
    static var smallRegularFont =  UIFont(name: FONTS.regular.rawValue , size: 14)
    static var mediumRegularFont = UIFont(name: FONTS.regular.rawValue , size: 16)
    static var largeRegularFont = UIFont(name: FONTS.regular.rawValue , size: 18)
    static var xlargeRegularFont =  UIFont(name: FONTS.regular.rawValue , size: 20)
    static var xxlargeRegularFont =  UIFont(name: FONTS.regular.rawValue , size: 24)
    
    static var smallBoldFont =  UIFont(name: FONTS.bold.rawValue , size: 14)
    static var mediumBoldFont = UIFont(name: FONTS.bold.rawValue , size: 16)
    static var largeBoldFont = UIFont(name: FONTS.bold.rawValue , size: 18)
    static var xlargeBoldFont =  UIFont(name: FONTS.bold.rawValue , size: 20)
    static var xxlargeBoldFont =  UIFont(name: FONTS.bold.rawValue , size: 24)
   
    static var verysmallSemiBoldFont =  UIFont(name: FONTS.medium.rawValue , size: 12)
    static var smallSemiBoldFont =  UIFont(name: FONTS.medium.rawValue , size: 14)
    static var mediumSemiBoldFont = UIFont(name: FONTS.medium.rawValue , size: 16)
    static var largeSemiBoldFont = UIFont(name: FONTS.medium.rawValue , size: 18)
    static var xlargeSemiBoldFont =  UIFont(name: FONTS.medium.rawValue , size: 20)
    static var xxlargeSemiBoldFont =  UIFont(name: FONTS.medium.rawValue , size: 24)
    
    
}


enum Languages {
    case ar
    case en
}

var appLanguage = Languages.en
