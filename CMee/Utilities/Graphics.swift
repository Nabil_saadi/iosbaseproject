//
//  Graphics.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
class Graphics {
    
    static func shake_view(view : UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x:view.center.x - 10,y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x:view.center.x + 10,y: view.center.y))
        view.layer.add(animation, forKey: "position")
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    static func show_message(viewController:UIViewController,title:String,message:String){
        let alert = UIAlertController(title: title.localized(), message:message.localized() , preferredStyle: UIAlertController.Style.alert)
        alert.setTitlet(font:  customFonts.mediumFont)
        alert.setMessage(font: customFonts.mediumFont)
        
        alert.addAction(UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
    static func show_message(viewController:UIViewController,title:String,message:String,completion:@escaping () -> Void){
        let alert = UIAlertController(title: title.localized(), message:message.localized() , preferredStyle: UIAlertController.Style.alert)
        alert.setTitlet(font:  customFonts.mediumFont )
        alert.setMessage(font: customFonts.mediumFont)
        alert.addAction(UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default, handler: { _ in
            completion()
        }))
        viewController.present(alert, animated: true, completion: {
            
        })
    }
    static func showActionMessage(viewController:UIViewController,title:String,message:String,OkCompletion: @escaping () -> Void){
        let alert = UIAlertController(title: title.localized(), message:message.localized() , preferredStyle: UIAlertController.Style.alert)
        alert.setTitlet(font:  customFonts.mediumFont )
        alert.setMessage(font: customFonts.mediumFont)
        let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            OkCompletion()
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            
        }
        
        
        // Add the actions
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    static func showTryAgainMessage(viewController:UIViewController,title:String,message:String,OkCompletion: @escaping () -> Void){
        let alert = UIAlertController(title: title.localized(), message:message.localized() , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Try Again".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            OkCompletion()
        }
        // Add the actions
        alert.addAction(okAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    static func comma_price(price:Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:price))
        return formattedNumber!
    }
//    static func showHUD(vc:UIViewController){
//        LoadingHUD.main.show(in: vc.view)
//    }
//    static func hideHUD(){
//        //errorView?.isHidden = true
//        LoadingHUD.main.dismiss()
//    }
}
