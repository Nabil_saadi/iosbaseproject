//
//  LocationHandler.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation

import UIKit
import CoreLocation
protocol locaionDelegate {
    func didChangeAuth()
}
class LocationHandler: NSObject, CLLocationManagerDelegate {
    var delegate : locaionDelegate?
    //    private static var __once: () = {
    //            Static.instance = LocationHandler()
    //        }()
    
    // MARK: - Properties
    //    static let sharedInstance = LocationHandler()
    var locationManager: CLLocationManager!
    
    // MARK: - Singleton Method
    
    static var sharedInstance = LocationHandler() //{
    //        struct Static {
    //            static var onceToken: Int = 0
    //            static var instance: LocationHandler? = nil
    //        }
    //
    ////        _ = LocationHandler.__once
    //        return Static.instance ?? LocationHandler()
    //    }
    //
    override init() {
        super.init()
        //        if locationManager != nil {
        //            locationManager.delegate = self
        //        }
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func initilize(){
        
        
    }
    
    // MARK: - Location Manager Delegate Methods
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        let locationArray = locations as NSArray
        //        let locationObj = locationArray.lastObject as! CLLocation
        //        let coord = locationObj.coordinate
        //
        //        print("Latitude: \(coord.latitude) & Longitude: \(coord.longitude)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
            var shouldIAllow = false
            var error = ""
    
            switch status {
            case CLAuthorizationStatus.restricted:
                error = "Restricted Access to location"
            case CLAuthorizationStatus.denied:
                error = "User denied access to location"
            case CLAuthorizationStatus.notDetermined:
                error = "Status not determined"
            default:
                error = "Allowed to location Access"
                delegate?.didChangeAuth()
                shouldIAllow = true
            }
    
            if shouldIAllow {
                print("Location to Allowed")
                locationManager.startUpdatingLocation()
            } else {
                print("Denied access: \(error)")
            }
    }
    
    // MARK: - Private Methods
    
    var isLocationServiceEnable: Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
        } else {
            // Location services are not enabled
            return false
        }
    }
    var location : CLLocationCoordinate2D = CLLocation(latitude: 0.0, longitude: 0.0).coordinate
    func getCurrentLocation() -> CLLocationCoordinate2D {
        //locationManager.startUpdatingLocation()
        //        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
        //            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
        if let location = locationManager.location {
            //            if location == nil {
            //                location = CLLocation(latitude: 0.0, longitude: 0.0)
            //            }
            self.location = location.coordinate
            return location.coordinate
        }
        return   CLLocation(latitude: 0.0, longitude: 0.0).coordinate
        //        }
        //        return CLLocation(latitude: 0.0, longitude: 0.0).coordinate
    }
    
    func getDistanceBetween(_ location1: CLLocationCoordinate2D, location2: CLLocationCoordinate2D) -> CLLocationDistance {
        let location1Tmp = CLLocation(latitude: location1.latitude, longitude: location1.longitude)
        let location2Tmp = CLLocation(latitude: location2.latitude, longitude: location2.longitude)
        return location1Tmp.distance(from: location2Tmp)
    }
    
    func getDistanceFromCurrentLocation(_ location: CLLocationCoordinate2D) -> CLLocationDistance {
        // Distance is in meter
        return self.getDistanceBetween(self.getCurrentLocation(), location2: location)
    }
    
}
