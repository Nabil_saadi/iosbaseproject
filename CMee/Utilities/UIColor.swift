//
//  UIColor.swift
//  CMee
//
//  Created by nabil on 6/9/20.
//  Copyright © 2020 nabil. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    // 1D7FE7   7B1979  FF7C00  FFD300   FF2600  3DA600
    static let companyStatusColors = [#colorLiteral(red: 0.1137254902, green: 0.4980392157, blue: 0.9058823529, alpha: 1),#colorLiteral(red: 0.4823529412, green: 0.09803921569, blue: 0.4745098039, alpha: 1),#colorLiteral(red: 1, green: 0.4862745098, blue: 0, alpha: 1),#colorLiteral(red: 1, green: 0.8274509804, blue: 0, alpha: 1),#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1),#colorLiteral(red: 0.2374400198, green: 0.6492436528, blue: 0, alpha: 1)]
    static let companyChangeStatusColors = [#colorLiteral(red: 1, green: 0.4862745098, blue: 0, alpha: 1),#colorLiteral(red: 1, green: 0.8274509804, blue: 0, alpha: 1),#colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1),#colorLiteral(red: 0.2374400198, green: 0.6492436528, blue: 0, alpha: 1)]
    static let AppointmentColors = [#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1) , #colorLiteral(red: 0.2374400198, green: 0.6492436528, blue: 0, alpha: 1) ,#colorLiteral(red: 0.968627451, green: 1, blue: 0.05490196078, alpha: 1) , #colorLiteral(red: 1, green: 0.3215686275, blue: 0.06666666667, alpha: 1) , #colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1),#colorLiteral(red: 0.1137254902, green: 0.4980392157, blue: 0.9058823529, alpha: 1) ]
    static let seaBlue = #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 1)
    static let blue = #colorLiteral(red: 0.1960784314, green: 0.6078431373, blue: 0.9450980392, alpha: 1)
    static let seaBlueAlpha = #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 0.6027178697)
    static let Red = #colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 1)
    static let redAlpha = #colorLiteral(red: 1, green: 0.1490196078, blue: 0, alpha: 0.6028199914)
    static let seaBlueAlpha2 = #colorLiteral(red: 0.213855654, green: 0.4607372284, blue: 0.7347461581, alpha: 0.5104258363)
    static let lightBurble = #colorLiteral(red: 0.8705882353, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
    static let bluegreen = #colorLiteral(red: 0.2352941176, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
    static let appleGreen = #colorLiteral(red: 0.2941176471, green: 0.5960784314, blue: 0.03137254902, alpha: 1)
    static let appleGreenDark = #colorLiteral(red: 0.3529411765, green: 0.5803921569, blue: 0.1568627451, alpha: 1)
    static let fire = #colorLiteral(red: 1, green: 0.3215686275, blue: 0.06666666667, alpha: 1)
    static let disableColor = UIColor(red: 33, green: 150, blue: 243, alpha: 0.30)
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}
